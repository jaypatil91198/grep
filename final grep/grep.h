#include <stdio.h>
#include <dirent.h>
typedef struct node {
	char *line;
	int n, byte;
	struct node *prev, *next;
}node;
typedef struct data {
	node *head, *tail;
}data;
typedef struct files{
	char file[128][64];
	char folder[128][64];
	int fi;
}files;
void init(data *d);
void addNode(data *d, char *line);
void storeData(FILE *fp, data *d);
void printData(data *d);
void find(data *d, char *word, int iflag, int wflag, int mflag, int mnum);
void print(data *d, int n, int cflag, int Hflag, char *file, int hflag, int qflag, int bflag, int mflag, int mnum);
void allfiles(files *f, char *folder);
